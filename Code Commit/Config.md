# Configuration de code commit

## Config via aws

faire:
```
git clone --config 'credential.helper=!aws codecommit --profile arkema-archiaws --region eu-west-3 credential-helper $@' --config 'credential.UseHttpPath=true' https://git-codecommit.eu-west-3.amazonaws.com/v1/repos/ArchiAWS
```

Dans .ssh/config
```
[profile arkema-archiaws] role_arn = arn:aws:iam::332741258372:role/ArchiAWSCodeCommitRole source_profile = prod mfa_serial = arn:aws:iam::332741258372:mfa/laurent.de-berti@arkema.com
```

## Configuration via git-remote-codecommit

Source: https://github.com/awslabs/git-remote-codecommit

This packages extends git to support repository urls prefixed with codecommit://

For example, if using IAM...


```
  % cat ~/.aws/config
  [profile demo]
  region = us-east-2
  account = 111122223333
```

```
  % cat ~/.aws/credentials
  [demo]
  aws_access_key_id = AKIAIOSFODNN7EXAMPLE
  aws_secret_access_key = wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
```
... you can clone repositories as simply as...

Clone the repo using:
```
git clone codecommit://demo@MyRepositoryName
```

